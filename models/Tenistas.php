<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenistas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $correo
 * @property int $altura
 * @property int $peso
 * @property string $fechBaja
 * @property int $activo
 * @property int $naciones
 *
 * @property Naciones $naciones0
 */
class Tenistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['altura', 'peso', 'activo', 'naciones'], 'integer'],
            [['fechBaja'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['correo'],'email'],
            [['naciones'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['naciones' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'altura' => 'Altura (cms)',
            'peso' => 'Peso(kg)',
            'fechBaja' => 'Fecha de Baja',
            'activo' => 'Activo',
            'naciones' => 'Naciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNaciones0()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'naciones']);
    }
}
